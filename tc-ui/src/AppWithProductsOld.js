import React, { Component } from "react";
import Products from "./Products/Products";
import nevada from "./Images/nevada.jpeg";
import chair from "./Images/chair.jpeg";
import apple from "./Images/apple.jpeg";
import ProductAdministrationComponent from "./components/testing/products/productAdministrationComponent";
import { Switch, Redirect, Route } from "react-router";
import { BrowserRouter, Link } from "react-router-dom";

var productDetails = [
  {
    imageUrl: nevada,
    title: "NEVADA222",
    description: "this is Nevada state!",
    price: 5
  },
  {
    imageUrl: nevada,
    title: "Chair",
    description: "this is Nevada state!",
    price: 61
  },
  {
    imageUrl: nevada,
    title: "Apple",
    description: "A deliciuous red apple",
    price: 3
  },
  {
    imageUrl: nevada,
    title: "NEVAD2",
    description: "this is Nevada state!",
    price: 5
  },
  {
    imageUrl: nevada,
    title: "Chair222",
    description: "this is Nevada state!",
    price: 64
  },
  {
    imageUrl: nevada,
    title: "Apple2",
    description: "A deliciuous red apple",
    price: 34
  }
];

const administrationProperties = {
  title: "titleAdmin",
  description: "descriptionAdmin",
  imageUrl: nevada,
  quantity: 43,
  price: 100
};

class App extends Component {
  handleSave = () => {
    console.log("InsideHandleSave in App");
  };

  goToProducts = () => this.props.history.push("products");
  render() {
    return (
      <div>
        <p>
          <button
            onClick={this.goToProducts}
            className="btn btn-primary"
            role="button"
          >
            Go to Products
          </button>
        </p>
        <ProductAdministrationComponent
          propertiesObject={administrationProperties}
          onSave={this.handleSave}
        />

        {/* <Products productsInputByUserArray={productDetails} /> */}
      </div>
    );
  }
}

export default App;
