import React, { Component } from "react";
import { Switch, Redirect, Route } from "react-router";
import { BrowserRouter, Link } from "react-router-dom";

const AppContainer = props => {
  return (
    <div>
      <div>
        <p>AppContainer</p>
        <Link to="/">Home</Link>&nbsp;
        <Link to="/studios">Studios</Link>&nbsp;
        <Link to="/studios/${127}">Studio by title</Link>&nbsp;
        <Link to="/help">Help</Link>&nbsp;
        <Link to="/non-existant">Non Existant</Link>&nbsp;
      </div>{" "}
      {props.children}
    </div>
  );
};

export default AppContainer;
