import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import * as serviceWorker from "./serviceWorker";
import "../node_modules/bootstrap/dist/css/bootstrap.min.css";
import { Switch, Redirect, Route } from "react-router";
import { BrowserRouter, Link } from "react-router-dom";
import NoMatch from "./components/navigation/NoMatch";
import AppContainer from "./AppContainer";
import NewProductContainer from "./components/productAdministration/NewProductContainer";
import ServicesContext from "./ServicesContext";
import UserService from "./components/userService/userService";
import EditProductContainer from "./components/productAdministration/EditProductContainer";
import ProductListComponent from "./components/productList/ProductListComponent";
import ProductAdministrationListContainer from "./components/productAdministration/ProductAdministrationListContainer";
import OneProductContainer from "./components/productList/OneProductContainer";

var DemonstruotiNavigacija = props => {
  var goHome = () => props.history.push("/");
  return (
    <div>
      At route:{props.location.pathname}
      <button onClick={goHome}>Go Home</button>
      <pre>{JSON.stringify(props, null, 2)}</pre>
    </div>
  );
};

// ReactDOM.render(<App />, document.getElementById("root"));
ReactDOM.render(
  <BrowserRouter>
    <ServicesContext.Provider
      value={{ userFromContext: { name: "Paulius pagaliau pasikeite" } }}
    >
      <AppContainer>
        <Switch>
          <Route exact path="/" component={App} />
          <Route exact path="/studios/:title" component={OneProductContainer} />

          <Route exact path="/studios" component={App} />
          <Route exact path="/help" component={DemonstruotiNavigacija} />
          <Route
            exact
            path="/admin"
            component={ProductAdministrationListContainer}
          />
          <Route exact path="/admin/new" component={NewProductContainer} />
          <Route path="*" component={NoMatch} />
          <Route component={NoMatch} />
        </Switch>
      </AppContainer>
    </ServicesContext.Provider>
  </BrowserRouter>,

  document.getElementById("root")
);
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
