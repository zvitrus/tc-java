import React, { Component } from "react";
import PropTypes from "prop-types";
import Product from "./Product";
import nevada from "../Images/nevada.jpeg";
import chair from "../Images/chair.jpeg";

class Products extends Component {
  // constructor() {
  //   super();
  //   this.state = {
  //     testingObject: {
  //       testingName: "name1",
  //       testingPrice: 43
  //     }
  //   };
  //   console.log(
  //     "testing price from a constructor: ",
  //     this.state.testingObject.testingPrice
  //   );
  // }
  // componentDidMount() {
  //   console.log("Component did mount");
  //   const object = {
  //     testingName: "name100",
  //     testingPrice: 100
  //   };
  //   this.setState({ testingObject: object });
  //   console.log(
  //     "testingObject  from componentDidMount",
  //     this.state.testingObject,
  //     "object: ",
  //     object
  //   );
  // }
  // static getDerivedStateFromProps() {
  //   console.log("Derived state");
  //   return null;
  // }
  render() {
    // console.log(
    //   "testingObject  from componentDidMount",
    //   this.state.testingObject
    // );
    var productsArrayToRender = this.props.productsInputByUserArray.map(
      object => {
        return (
          <div className="col-sm-3" key={object.title}>
            <Product
              imageUrl={object.imageUrl}
              title={object.title}
              description={object.description}
              price={object.price}
            />
          </div>
        );
      }
    );

    return (
      <div className="container-fluid">
        <div className="row">{productsArrayToRender}</div>
      </div>
    );

    Products.propTypes = {
      productsInpumtByUserArray: PropTypes.array.isRequired
    };

    // return (
    //   <div className="container-fluid">
    //     <div className="row">
    //       <div className="col-sm">
    //         <Product
    //           imageUrl={nevada}
    //           title="NEVADA"
    //           description="this is Nevada state!"
    //           price={5}
    //         />
    //       </div>

    //       <div className="col-sm">
    //         <Product
    //           imageUrl={chair}
    //           title="A Chair"
    //           description="This is a very nice wooden chair"
    //           price={10}
    //         />
    //       </div>
    //     </div>
    //   </div>
    // );
  }
}

export default Products;
