import React, { Component } from "react";
import PropTypes from "prop-types";

var styles = {
  card: {
    width: "18rem",
    height: "18rem"
  },
  img: {}
};

class ProductCardComponent extends Component {
  render() {
    return (
      <div className="card m-5 " style={styles.card}>
        <img
          style={styles.img}
          className="card-img-top"
          src={this.props.imageUrl}
          alt={this.props.title}
        />
        <div className="card-body">
          <h5 className="card-title">{this.props.title}</h5>
          <p className="card-text">{this.props.description}</p>
          <p className="card-text">{this.props.price}</p>
          <a href="#" className="btn btn-primary">
            Go somewhere
          </a>
        </div>
      </div>
    );
  }
}

ProductCardComponent.propTypes = {
  imageUrl: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired
};

export default ProductCardComponent;
