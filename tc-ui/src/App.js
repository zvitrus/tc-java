import React, { Component } from "react";
import { Switch, Redirect, Route } from "react-router";
import { BrowserRouter, Link } from "react-router-dom";
import Komponentas from "./components/testing/komponentasComponent";
import NameForm from "./components/testing/nameForm";
import KomponentasContainer from "./components/testing/komponentasContainer";
import ProductListComponent from "./components/productList/ProductListComponent";
import ServicesContext from "./ServicesContext";
import UserService from "./components/userService/userService";
import EditProductComponent from "./components/productAdministration/EditProductComponent";

class App extends Component {
  goProducts = () => {
    this.props.history.push("studios");
  };

  render() {
    return (
      <div>
        <button onClick={this.goProducts} className="btn btn-primary">
          Go To Studios
        </button>
        <UserService />
        {/* <EditProductComponent /> */}
        <ProductListComponent />
      </div>
    );
  }
}

export default App;
