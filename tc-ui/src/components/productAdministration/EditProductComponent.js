import React, { Component } from "react";
import PropTypes from "prop-types";

// var styles = {
//   card: {
//     width: "18rem",
//     height: "18rem"
//   },
//   img: {}
// };

const EditProductComponent = props => {
  return (
    <div>
      <form>
        <p>Enter product details to upadate</p>
        <div className="row m-4">
          <div className="col-6">
            <input
              type="text"
              className="form-control"
              placeholder="Title"
              onChange={props.onChangeTitle}
            />
          </div>
        </div>
        <div className="row m-4">
          <div className="col">
            <input
              type="text"
              className="form-control"
              placeholder="image URL"
              onChange={props.onChangeImageUrl}
            />
          </div>
          <div className="col">
            <input
              type="text"
              className="form-control"
              placeholder="Description"
              onChange={props.onChangeDescription}
            />
          </div>
        </div>
        <div className="row m-4">
          <div className="col">
            <input
              type="number"
              className="form-control"
              placeholder="Price"
              onChange={props.onChangePrice}
            />
          </div>
          <div className="col">
            <input
              type="number"
              className="form-control"
              placeholder="Quantity"
              onChange={props.onChangeQuantity}
            />
          </div>
        </div>
        <input className="btn btn-primary" type="submit" value="Update" />{" "}
        <button className="btn btn-danger" onClick={props.handleDelete}>
          Delete
        </button>
      </form>
    </div>
  );
};
// ProductAdministrationComponent.propTypes = {

//   imageUrl: PropTypes.string.isRequired,
//   title: PropTypes.string.isRequired,
//   description: PropTypes.string.isRequired,
//   price: PropTypes.number.isRequired,
//   quantity: PropTypes.number.isRequired
// };
export default EditProductComponent;
