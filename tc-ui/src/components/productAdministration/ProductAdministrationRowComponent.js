import React from "react";
import PropTypes from "prop-types";
import nevada from "../../Images/nevada.jpeg";
import { withRouter } from "react-router";

//ar reikia jo? Ar pades jis?
import { Link } from "react-router-dom";

const productAdministrationRowComponent = props => {
  var linkas = "/admin/products/" + props.studioTitle;
  return (
    <div className="container">
      <div className="row">
        <div className="col-2">
          <Link to={linkas}>{props.studioTitle}</Link>
        </div>
        <div className="col-2">
          <img src={props.logo} alt={props.studioTitle} width="50px" />
        </div>
        <div className="col-8">
          <p>{props.category}</p>
        </div>
      </div>
    </div>
  );
};

export default withRouter(productAdministrationRowComponent);
