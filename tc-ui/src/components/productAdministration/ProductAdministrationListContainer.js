import React from "react";
import PropTypes from "prop-types";
import ProductAdministrationRowComponent from "./ProductAdministrationRowComponent";
import axios from "axios";
import { Link } from "react-router-dom";
import nevada from "../../Images/nevada.jpeg";

class ProductAdministrationListContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      products: "",
      // products: [
      //   {
      //     logo: nevada,
      //     studioTitle: "NEVADA222",
      //     category: "this is Nevada state!",
      //     size: "mazas"
      //   },
      //   {
      //     logo: nevada,
      //     studioTitle: "NEVADA222",
      //     category: "this is Nevada state!",
      //     size: "mazas"
      //   }
      // ],
      loading: "Studios are being loaded..."
    };
  }

  componentDidMount() {
    axios
      .get("http://localhost:8081/studios")
      .then(response => {
        this.setState({ products: response.data });
        //console.log(response.data);
        //console.log("Produktai yra - " + this.state.holidays);
      })
      .catch(error => {
        console.log(error);
      });
  }

  render() {
    if (this.state.products) {
      const productCards = this.state.products.map((product, index) => {
        return (
          <ProductAdministrationRowComponent
            key={index}
            studioTitle={product.studioTitle}
            logo={product.logo}
            category={product.category}
          />
        );
      });
      return (
        <div className="container">
          <div className="row">
            <Link className="btn btn-success" to="/admin/products/new">
              Add new studio
            </Link>
          </div>
          <div className="row">
            <div className="col-2">
              <p>Studio</p>
            </div>
            <div className="col-2">
              <p>Logo</p>
            </div>
            <div className="col-8">
              <p>Category</p>
            </div>
          </div>
          <div className="row">{productCards}</div>
        </div>
      );
    }
    return this.state.loading;
  }
}

export default ProductAdministrationListContainer;
