import React from "react";
import axios from "axios";
import { withRouter } from "react-router";
import EditProductComponent from "./EditProductComponent";

class EditProductContainer extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      oldStudioTitle: "",
      studioTitle: "",
      logo: "",
      category: "",
      size: ""
    };
  }

  componentDidMount() {
    const position = this.props.match.params.StudioTitle;

    axios
      .get("http://localhost:8081/studios/" + position)
      .then(response => {
        this.setState({ oldStudioTitle: response.data.studioTitle });
        this.setState({ studioTitle: response.data.studioTitle });
        this.setState({ logo: response.data.logo });
        this.setState({ category: response.data.category });
        this.setState({ size: response.data.size });
      })
      .catch(error => {
        console.log(error);
      });
  }

  handleChangeStudioTitle = event => {
    this.setState({ title: event.target.value });
    console.log(this.state.title);
  };

  handleChangeLogo = event => {
    this.setState({ image: event.target.value });
  };

  handleChangeCategory = event => {
    this.setState({ description: event.target.value });
  };

  handleChangeSize = event => {
    this.setState({ type: event.target.value });
  };

  handleSubmit = event => {
    event.preventDefault();
    axios
      .put(
        "http://localhost:8080/studios/" + this.state.oldStudioTitle,
        this.state
      )

      .then(function(response) {})
      .catch(function(error) {
        console.log(error);
      });
  };

  handleDelete = event => {
    event.preventDefault();
    axios
      .delete("http://localhost:8081/studios/" + this.state.oldStudioTitle)
      .then(response => {
        console.log("Deleted");
      })
      .catch(function(error) {
        console.log(error);
      });
  };

  render() {
    return (
      <EditProductComponent
        handleChangeStudioTitle={this.handleChangeStudioTitle}
        handleChangeLogo={this.handleChangeLogo}
        handleChangeCategory={this.handleChangeCategory}
        handleChangeSize={this.handleChangeSize}
        handleSubmit={this.handleSubmit}
        handleDelete={this.handleDelete}
        fromMenu={this.fromMenu}
        currentStudioTitle={this.state.studioTitle}
        currentLogo={this.state.logo}
        currentCategory={this.state.category}
        currentSize={this.state.size}
      />
    );
  }
}

export default withRouter(EditProductContainer);
