import React, { Component } from "react";
import PropTypes from "prop-types";
import NewProductComponent from "./NewProductComponent";
import axios from "axios";
import nevada from "../../Images/nevada.jpeg";
import { withRouter } from "react-router";

class NewProductContainer extends Component {
  constructor() {
    super();
    this.state = {
      value: {
        studioTitle: "",
        logo: { nevada },
        category: "",
        size: ""
      }
    };
  }

  handleChangeStudioTitle = event => {
    const newStateValue = this.state.value;
    newStateValue.studioTitle = event.target.value;

    this.setState({
      value: newStateValue
    });
  };

  handleChangeLogo = event => {
    const newStateValue = this.state.value;
    newStateValue.logo = event.target.value;

    this.setState({
      value: newStateValue
    });
  };
  handleChangeCategory = event => {
    const newStateValue = this.state.value;
    newStateValue.category = event.target.value;

    this.setState({
      value: newStateValue
    });
  };
  handleChangeSize = event => {
    const newStateValue = this.state.value;
    newStateValue.size = event.target.value;

    this.setState({
      value: newStateValue
    });
  };
  // handleChangeQuantity = event => {
  //   const newStateValue = this.state.value;
  //   newStateValue.quantity = event.target.value;

  //   this.setState({
  //     value: newStateValue
  //   });
  // };
  handleSubmit = event => {
    event.preventDefault();
    axios
      .post("https://localhost:8081/studios", this.state)
      .then(response => {
        console.log(response);
      })
      .catch(error => {
        console.log(error);
      });
  };
  // handleSubmit = event => {
  //   this.setState({
  //     value: {
  //       title: "inside handleSubmit"
  //     }
  //   });
  //   event.preventDefault();
  // };
  render() {
    console.log(this.state.value);
    return (
      <NewProductComponent
        propertiesObject={this.state}
        onSubmit={this.handleSubmit}
        onStudioTitleChange={this.handleChangeStudioTitle}
        onLogoChange={this.handleChangeLogo}
        onCategoryChange={this.handleChangeCategory}
        onPriceSize={this.handleChangeSize}
      />
    );
  }
}

export default withRouter(NewProductContainer);
