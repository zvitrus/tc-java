import React, { Component } from "react";
import PropTypes from "prop-types";

// var styles = {
//   card: {
//     width: "18rem",
//     height: "18rem"
//   },
//   img: {}
// };

const ProductAdministrationComponent = props => {
  return (
    <div>
      <form>
        {/* Name{props.propertiesObject.value.title}:
        <br />
        <input
          type="text"
          value={props.propertiesObject.value.title}
          onChange={props.onTitleChange}
        /> */}
        <p>Enter studio's details to create a new studio</p>
        <div className="row m-4">
          <div className="col-6">
            <input
              type="text"
              className="form-control"
              // value={this.state.value.imageUrl}
              placeholder="Title"
              // value={this.state.value.imageUrl}
              onChange={props.onStudioTitleChange}
            />
          </div>
        </div>
        <div className="row m-4">
          <div className="col">
            <input
              type="text"
              className="form-control"
              placeholder="logo URL"
              // value={this.state.value.imageUrl}
              onChange={props.onLogoChange}
            />
          </div>
          <div className="col">
            <input
              type="text"
              className="form-control"
              placeholder="Category"
              onChange={props.onCategoryChange}
            />
          </div>
        </div>
        <div className="row m-4">
          <div className="col">
            <input
              type="number"
              className="form-control"
              placeholder="Size"
              onChange={props.onSizeChange}
            />
          </div>
          {/* <div className="col">
            <input
              type="number"
              className="form-control"
              placeholder="Quantity"
              onChange={props.onQuantityChange}
            />
          </div> */}
        </div>
        <input type="submit" value="submit" />
      </form>
    </div>
  );
};
// ProductAdministrationComponent.propTypes = {

//   imageUrl: PropTypes.string.isRequired,
//   title: PropTypes.string.isRequired,
//   description: PropTypes.string.isRequired,
//   price: PropTypes.number.isRequired,
//   quantity: PropTypes.number.isRequired
// };
export default ProductAdministrationComponent;
