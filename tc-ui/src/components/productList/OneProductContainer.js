import React, { Component } from "react";
import PropTypes from "prop-types";
import OneProductComponent from "./OneProductComponent";
import axios from "axios";
import UserContext from "../../ServicesContext";

class OneProductContainer extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      studioTitle: "",
      logo: "",
      category: "",
      size: ""
    };
  }

  componentDidMount() {
    const position = this.props.match.params.studioTitle;
    axios
      .get("http://localhost:8081/studios/" + position)
      .then(response => {
        this.setState({
          title: response.data.studioTitle,
          logo: response.data.logo,
          category: response.data.category,
          size: response.data.size
        });
      })
      .catch(error => {
        console.log(error);
      });
  }

  render() {
    return (
      <div>
        <OneProductComponent
          studioTitle={this.state.studioTitle}
          logo={this.state.logo}
          category={this.state.category}
          size={this.state.size}
        />
      </div>
    );
  }
}

export default OneProductContainer;
