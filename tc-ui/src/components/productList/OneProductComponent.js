import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import nevada from "../../Images/nevada.jpeg";

const OneProductComponent = props => {
  return (
    <div className="container">
      <div className="row">
        <div className="col-3">
          <img src={props.logo} alt={props.studioTitle} width="150px" />
        </div>
        <div className="col-3">
          <h4>{props.studioTitle}</h4>
          <p>{props.category}</p>
          <p>{props.size}</p>
        </div>
      </div>
    </div>
  );
};

/*
OneProductComponent.propTypes = {
    id:PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
    image: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    quantity: PropTypes.number.isRequired
};*/

export default OneProductComponent;
