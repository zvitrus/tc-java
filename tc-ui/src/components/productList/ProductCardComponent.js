import React, { Component } from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

var styles = {
  card: {
    width: "18rem",
    height: "18rem"
  },
  img: {}
};
const OneProductComponent = props => {
  var linkToDetails = "/studios/" + props.studioTitle;

  return (
    <div className="card m-5 " style={styles.card}>
      <img
        style={styles.img}
        className="card-img-top"
        src={props.logo}
        alt={props.studioTitle}
      />
      <div className="card-body">
        <h5 className="card-title">{props.studioTitle}</h5>
        <p className="card-text">{props.category}</p>
        <p className="card-text">Size: {props.size}</p>
        {/* <p className="card-text">Quantity: {props.quantity}</p> */}
        <Link className="btn btn-primary" to={linkToDetails}>
          Studio details
        </Link>
      </div>
    </div>
  );
};

// class OneProductComponent extends Component {
//   render() {
//     return (
//       <div className="card m-5 " style={styles.card}>
//         <img
//           style={styles.img}
//           className="card-img-top"
//           src={this.props.imageUrl}
//           alt={this.props.title}
//         />
//         <div className="card-body">
//           <h5 className="card-title">{this.props.title}</h5>
//           <p className="card-text">{this.props.description}</p>
//           <p className="card-text">{this.props.price}</p>
//           <p className="card-text">{this.props.quantity}</p>
//           <a href="#" className="btn btn-primary">
//             Go somewhere
//           </a>
//         </div>
//       </div>
//     );
//   }

// OneProductComponent.propTypes = {
//   imageUrl: PropTypes.string.isRequired,
//   title: PropTypes.string.isRequired,
//   description: PropTypes.string.isRequired,
//   price: PropTypes.number.isRequired
// };

export default OneProductComponent;
