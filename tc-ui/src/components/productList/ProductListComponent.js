import React, { Component } from "react";
import PropTypes from "prop-types";
import nevada from "../../Images/nevada.jpeg";
import ProductCardComponent from "./ProductCardComponent";
import axios from "axios";

var styles = {
  card: {
    width: "18rem",
    height: "18rem"
  },
  img: {}
};

class ProductListComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      productsInfoArray: [
        {
          logo: nevada,
          studioTitle: "NEVADA222 ProductListComponent",
          category: "this is Nevada state!",
          size: 5
        },
        {
          logo: nevada,
          studioTitle: "NEVADA222dsdsds",
          category: "this is Nevada state!",
          size: 5
        }
      ]
    };
  }

  componentDidMount() {
    axios
      .get("https://localhost:8081/studios")
      .then(response => {
        this.setState({ productsInfoArray: response.data });
      })
      .catch(error => {
        console.log(error);
      });
  }
  render() {
    var productsArrayToRender = this.state.productsInfoArray.map(oneObject => {
      return (
        <div className="col-sm-3" key={oneObject.studioTitle}>
          <ProductCardComponent
            studioTitle={oneObject.studioTitle}
            logo={oneObject.logo}
            category={oneObject.category}
            size={oneObject.size}
          />
        </div>
      );
    });

    return (
      <div className="container-fluid">
        <div className="row">{productsArrayToRender}</div>
      </div>
    );
  }
}

// ProductListComponent.propTypes = {
//   imageUrl: PropTypes.string.isRequired,
//   title: PropTypes.string.isRequired,
//   description: PropTypes.string.isRequired,
//   price: PropTypes.number.isRequired
// };

export default ProductListComponent;
