import React, { Component } from "react";
import PropTypes from "prop-types";

const KomponentasComponent = props => {
  return (
    <div>
      <p>State value is: {props.value}</p>
    </div>
  );
};

export default KomponentasComponent;
