import React, { Component } from "react";
import PropTypes from "prop-types";
import KomponentasComponent from "./komponentasComponent";

class KomponentasContainer extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = { value: "" };
  }

  componentDidMount() {
    this.setState({ value: "Value was set in componentDidMount method" });
  }
  render() {
    return <KomponentasComponent value={this.state.value} />;
  }
}

export default KomponentasContainer;
