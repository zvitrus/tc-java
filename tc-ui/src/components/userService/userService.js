import React, { Component } from "react";
import PropTypes from "prop-types";
import ServicesContext from "../../ServicesContext";

class UserService extends Component {
  state = {
    user: {
      name: "Name1",
      age: 54
    }
  };
  render() {
    return (
      <ServicesContext.Consumer>
        {/* //userNameObject actually is the value from the Context */}
        {userNameObject => {
          return <span> {userNameObject.userFromContext.name}</span>;
        }}
      </ServicesContext.Consumer>
    );
  }
}

export default UserService;
