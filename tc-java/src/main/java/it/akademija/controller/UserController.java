package it.akademija.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import it.akademija.dao.UserRepository;
import it.akademija.model.CreateUserCommand;
import it.akademija.model.User;
import it.akademija.model.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@Api(value = "user")
public class UserController {

    private UserService userService;
    private PagingData pagingData;
    @Autowired
    private CreateUserCommand createUserCommand;
//  private UserDao userDao;


    @Autowired()
    public UserController(UserService userService, PagingData pagingData) {
        this.userService = userService;
        this.pagingData = pagingData;

    }


    @RequestMapping(method = RequestMethod.GET, path = "/api/users")
    @ApiOperation(value = "Get users bla bla", notes = "Return registered users bla bla new project")
    public List<User> getUsers() {
        this.pagingData.setLimit(10);
        return userService.getUsers();
    }

    @RequestMapping(method = RequestMethod.POST, value = "/api/users")
    @ResponseStatus(HttpStatus.CREATED)
    @ApiOperation(value = "Create a new user", notes = "Creates a new user")
    public void addUser(@ApiParam(value = "User Data viens du trys", required = true)
                        @Valid @RequestBody final CreateUserCommand cmd) {
        userService.createUser(cmd);
    }
    @RequestMapping(method = RequestMethod.GET, path = "/api/users/{email}")
    @ApiOperation(value = "find user by email", notes = "Provide the user's email address")
    public User getUserByEmail( @PathVariable @RequestBody final String email){
        return userService.getUserByEmail(email);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/api/users/{username}")
    @ApiOperation(value = "Delete user", notes = "Enter the username of the user to delete it")
    public void deleteUser(@PathVariable @RequestBody final String username){
        userService.removeUser(username);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/api/users/{firstName}/{lastName}")
    @ApiOperation(value = "find user by firs name and last name", notes = "enter the first name and the last name")
    public User getUserByFirstNameAndLastName(@PathVariable @RequestBody  final String firstName,
                                              @PathVariable @RequestBody  final String lastName){
        return userService.getUserByFirstNameAndLastName(firstName, lastName);
    }
    @RequestMapping(method = RequestMethod.GET, value = "/api/users/emails/{partOfEmail}")
    @ApiOperation(value = "Find users by part of an email", notes = "enter the part of an email")
    public List<User> getUsersByEmailContaining(@PathVariable @RequestBody final String partOfEmail){
        return userService.getUsersByPartOfEmail(partOfEmail);
    }

//    @RequestMapping(method = RequestMethod.GET, value = "/api/users/{username}")
//    @ApiOperation(value = "Get a particular user", notes = "Return registered user searched by a user name ")
//    public User getUser(@PathVariable final String username){
////        return userDao.getUsers().stream().filter(user -> user.getStudioTitle().equals(username).findFirst().get());
//        User userToReturn = null;
//        for (User user: userDao.getUsers()){
//            if(user.getStudioTitle().equals(username)){
//                userToReturn =  user;
//
//            }
//        }
//        return userToReturn;
//    }


//    @RequestMapping(method = RequestMethod.DELETE, path = "/api/users/{username}")
//    @ResponseStatus(HttpStatus.NO_CONTENT)
//    @ApiOperation(value = "Delete a user", notes = "Enter the user name to delete the user")
//    public void deleteUser(@PathVariable final String username){
//        userDao.deleteUser(username);
//    }


    //this was used before UserDao had been created
//    @RequestMapping(method = RequestMethod.GET, value = "/api/users")
//    public List<User> getUsers() {
//        return Collections.emptyList();
//    }

//    @RequestMapping(method = RequestMethod.POST, value = "/api/users")
//    @ResponseStatus(HttpStatus.CREATED)
//    public void createUser (@RequestBody final CreateUserCommand cmd){
//        System.out.println(cmd);
//    }
//
//    @RequestMapping(method = RequestMethod.DELETE, path = "/api/users/{username}")
//    @ResponseStatus(HttpStatus.NO_CONTENT)
//    public void deleteUser(@PathVariable final String username){
//        System.out.println("Deleting user " + username);
//    }

}
