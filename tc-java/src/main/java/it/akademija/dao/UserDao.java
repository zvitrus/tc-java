package it.akademija.dao;

import it.akademija.model.User;

import java.util.List;

public interface UserDao {
    List<User> getUsers();
    void createUser(User user);
    void deleteUser(String username);

}
