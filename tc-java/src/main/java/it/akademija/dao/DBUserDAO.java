package it.akademija.dao;

import it.akademija.controller.PagingData;
import it.akademija.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
@Qualifier("repoUserDao")
public class DBUserDAO implements UserDao{

    @PersistenceContext
    private EntityManager entityManager;

    private PagingData pagingData;

    @Autowired
    public DBUserDAO(PagingData pagingData){
        this.pagingData = pagingData;
    }

    @Override
    public List<User> getUsers() {

        return entityManager.createQuery("SELECT u from User u", User.class).setMaxResults(pagingData.getLimit()).getResultList();
    }

    @Override
    public void createUser(User user) {
        entityManager.persist(user);
    }

    @Override
    public void deleteUser(String username) {
        User user = entityManager.createQuery("SELECT u from User u where esername = :un", User.class)
                .setParameter("un", username).getSingleResult();
        if (entityManager.contains(user)){
            entityManager.remove(user);
        }else{
            entityManager.remove(entityManager.merge(user));
        }
    }
}

