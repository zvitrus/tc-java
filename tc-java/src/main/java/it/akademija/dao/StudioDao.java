package it.akademija.dao;

import it.akademija.model.Studio;

import java.util.List;

public interface StudioDao {
    List<Studio> getStudios();
    void createStudio(Studio studio);
    void deleteStudio(String studioTitle);

}
