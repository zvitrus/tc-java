package it.akademija.dao;

import it.akademija.controller.PagingData;
import it.akademija.model.Studio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
@Qualifier("repoStudioDao")
public class DBStudioDAO implements StudioDao {

    @PersistenceContext
    private EntityManager entityManager;

    private PagingData pagingData;

    @Autowired
    public DBStudioDAO(PagingData pagingData) {
        this.pagingData = pagingData;
    }

    @Override
    public List<Studio> getStudios() {

        return entityManager.createQuery("SELECT u from Studio u", Studio.class).setMaxResults(pagingData.getLimit())
                .getResultList();
    }

    @Override
    public void createStudio(Studio studio) {
        entityManager.persist(studio);
    }

    @Override
    public void deleteStudio(String studioTitle) {
        Studio studio = entityManager.createQuery("SELECT u from Studio u where studioTitle = :un", Studio.class)
                .setParameter("un", studioTitle).getSingleResult();
        if (entityManager.contains(studio)) {
            entityManager.remove(studio);
        } else {
            entityManager.remove(entityManager.merge(studio));
        }
    }
}

