package it.akademija.model;

import org.hibernate.validator.constraints.Length;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

@Component

public class CreateStudioCommand {


    @NotNull
    @Length(min = 1, max = 30)
    private String studioTitle;
    @NotNull
    private String studioLogo;
    @NotNull
    @Length(min = 1, max = 30)
    private String studioCategory;
    @NotNull
    @Length(min = 1, max = 30)

    private String studioSize;

    public String getStudioTitle() {
        return studioTitle;
    }

    public void setStudioTitle(String studioTitle) {
        this.studioTitle = studioTitle;
    }

    public String getStudioLogo() {
        return studioLogo;
    }

    public void setStudioLogo(String studioLogo) {
        this.studioLogo = studioLogo;
    }

    public String getStudioCategory() {
        return studioCategory;
    }

    public void setStudioCategory(String studioCategory) {
        this.studioCategory = studioCategory;
    }

    public String getStudioSize() {
        return studioSize;
    }

    public void setStudioSize(String studioSize) {
        this.studioSize = studioSize;
    }
}
